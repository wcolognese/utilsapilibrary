﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UtilsTest
{
    public partial class UtilsTestForm : Form
    {
        public UtilsTestForm()
        {
            InitializeComponent();

            KeyboardForm keyboardForm = new KeyboardForm();

            tabKeyboard.Controls.Add(keyboardForm.GetPanel());

            MonitorForm monitorForm = new MonitorForm();

            tabMonitor.Controls.Add(monitorForm.GetPanel());
        }
    }
}
