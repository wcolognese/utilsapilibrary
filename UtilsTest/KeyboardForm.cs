﻿using System;
using System.Windows.Forms;
using UtilsAPILibrary;

namespace UtilsTest
{
    public partial class KeyboardForm : Form
    {
        Keyboard keyboard;

        public KeyboardForm()
        {
            InitializeComponent();

            this.keyboard = new Keyboard(this.Handle);
            this.Panel = panel;
        }

        public Panel GetPanel()
        {
            return this.Panel;
        }

        private void bt_play_Click(object sender, EventArgs e)
        {
            keyboard.Press.Play();
        }

        private void bt_stop_Click(object sender, EventArgs e)
        {
            keyboard.Press.Stop();
        }

        private void bt_pause_Click(object sender, EventArgs e)
        {
            keyboard.Press.Pause();
        }

        private void bt_next_Click(object sender, EventArgs e)
        {
            keyboard.Press.NextTrack();
        }

        private void bt_previous_Click(object sender, EventArgs e)
        {
            keyboard.Press.PreviousTrack();
        }

        private void bt_playPause_Click(object sender, EventArgs e)
        {
            keyboard.Press.PlayPause();
        }

        private void bt_mute_Click(object sender, EventArgs e)
        {
            keyboard.Press.Mute();
        }
    }
}
