﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace UtilsTest
{
    public partial class MonitorForm : Form
    {
        UtilsAPILibrary.Monitor monitor;

        public MonitorForm()
        {
            InitializeComponent();

            monitor = new UtilsAPILibrary.Monitor(this.Handle);
        }

        public Panel GetPanel()
        {
            return this.Panel;
        }

        private void bt_off_Click(object sender, EventArgs e)
        {
            monitor.SetMonitorState(UtilsAPILibrary.Monitor.MonitorState.OFF);

            Thread.Sleep(5000);

            monitor.SetMonitorState(UtilsAPILibrary.Monitor.MonitorState.ON);
        }
    }
}
