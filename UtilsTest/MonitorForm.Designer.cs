﻿namespace UtilsTest
{
    partial class MonitorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel = new System.Windows.Forms.Panel();
            this.lb_info = new System.Windows.Forms.Label();
            this.bt_off = new System.Windows.Forms.Button();
            this.Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.Controls.Add(this.lb_info);
            this.Panel.Controls.Add(this.bt_off);
            this.Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel.Location = new System.Drawing.Point(0, 0);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(284, 262);
            this.Panel.TabIndex = 0;
            // 
            // lb_info
            // 
            this.lb_info.Location = new System.Drawing.Point(37, 160);
            this.lb_info.Name = "lb_info";
            this.lb_info.Size = new System.Drawing.Size(206, 60);
            this.lb_info.TabIndex = 1;
            this.lb_info.Text = "After clicks on Button Off the monitor turn on in 5 seconds";
            // 
            // bt_off
            // 
            this.bt_off.Location = new System.Drawing.Point(100, 116);
            this.bt_off.Name = "bt_off";
            this.bt_off.Size = new System.Drawing.Size(75, 23);
            this.bt_off.TabIndex = 0;
            this.bt_off.Text = "Off";
            this.bt_off.UseVisualStyleBackColor = true;
            this.bt_off.Click += new System.EventHandler(this.bt_off_Click);
            // 
            // MonitorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MonitorForm";
            this.Text = "MonitorForm";
            this.Panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Label lb_info;
        private System.Windows.Forms.Button bt_off;
    }
}