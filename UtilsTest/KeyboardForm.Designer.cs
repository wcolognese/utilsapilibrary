﻿namespace UtilsTest
{
    partial class KeyboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new System.Windows.Forms.Panel();
            this.gb_media = new System.Windows.Forms.GroupBox();
            this.bt_play = new System.Windows.Forms.Button();
            this.bt_next = new System.Windows.Forms.Button();
            this.bt_stop = new System.Windows.Forms.Button();
            this.bt_previous = new System.Windows.Forms.Button();
            this.bt_pause = new System.Windows.Forms.Button();
            this.bt_playPause = new System.Windows.Forms.Button();
            this.bt_mute = new System.Windows.Forms.Button();
            this.panel.SuspendLayout();
            this.gb_media.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.gb_media);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(284, 262);
            this.panel.TabIndex = 0;
            // 
            // gb_media
            // 
            this.gb_media.Controls.Add(this.bt_mute);
            this.gb_media.Controls.Add(this.bt_playPause);
            this.gb_media.Controls.Add(this.bt_play);
            this.gb_media.Controls.Add(this.bt_next);
            this.gb_media.Controls.Add(this.bt_stop);
            this.gb_media.Controls.Add(this.bt_previous);
            this.gb_media.Controls.Add(this.bt_pause);
            this.gb_media.Location = new System.Drawing.Point(12, 12);
            this.gb_media.Name = "gb_media";
            this.gb_media.Size = new System.Drawing.Size(260, 198);
            this.gb_media.TabIndex = 5;
            this.gb_media.TabStop = false;
            this.gb_media.Text = "Media";
            // 
            // bt_play
            // 
            this.bt_play.Location = new System.Drawing.Point(29, 19);
            this.bt_play.Name = "bt_play";
            this.bt_play.Size = new System.Drawing.Size(75, 23);
            this.bt_play.TabIndex = 0;
            this.bt_play.Text = "Play";
            this.bt_play.UseVisualStyleBackColor = true;
            this.bt_play.Click += new System.EventHandler(this.bt_play_Click);
            // 
            // bt_next
            // 
            this.bt_next.Location = new System.Drawing.Point(145, 111);
            this.bt_next.Name = "bt_next";
            this.bt_next.Size = new System.Drawing.Size(75, 23);
            this.bt_next.TabIndex = 4;
            this.bt_next.Text = "Next";
            this.bt_next.UseVisualStyleBackColor = true;
            this.bt_next.Click += new System.EventHandler(this.bt_next_Click);
            // 
            // bt_stop
            // 
            this.bt_stop.Location = new System.Drawing.Point(29, 66);
            this.bt_stop.Name = "bt_stop";
            this.bt_stop.Size = new System.Drawing.Size(75, 23);
            this.bt_stop.TabIndex = 1;
            this.bt_stop.Text = "Stop";
            this.bt_stop.UseVisualStyleBackColor = true;
            this.bt_stop.Click += new System.EventHandler(this.bt_stop_Click);
            // 
            // bt_previous
            // 
            this.bt_previous.Location = new System.Drawing.Point(29, 111);
            this.bt_previous.Name = "bt_previous";
            this.bt_previous.Size = new System.Drawing.Size(75, 23);
            this.bt_previous.TabIndex = 3;
            this.bt_previous.Text = "Previous";
            this.bt_previous.UseVisualStyleBackColor = true;
            this.bt_previous.Click += new System.EventHandler(this.bt_previous_Click);
            // 
            // bt_pause
            // 
            this.bt_pause.Location = new System.Drawing.Point(145, 66);
            this.bt_pause.Name = "bt_pause";
            this.bt_pause.Size = new System.Drawing.Size(75, 23);
            this.bt_pause.TabIndex = 2;
            this.bt_pause.Text = "Pause";
            this.bt_pause.UseVisualStyleBackColor = true;
            this.bt_pause.Click += new System.EventHandler(this.bt_pause_Click);
            // 
            // bt_playPause
            // 
            this.bt_playPause.Location = new System.Drawing.Point(145, 19);
            this.bt_playPause.Name = "bt_playPause";
            this.bt_playPause.Size = new System.Drawing.Size(75, 23);
            this.bt_playPause.TabIndex = 5;
            this.bt_playPause.Text = "Play/Pause";
            this.bt_playPause.UseVisualStyleBackColor = true;
            this.bt_playPause.Click += new System.EventHandler(this.bt_playPause_Click);
            // 
            // bt_mute
            // 
            this.bt_mute.Location = new System.Drawing.Point(29, 156);
            this.bt_mute.Name = "bt_mute";
            this.bt_mute.Size = new System.Drawing.Size(75, 23);
            this.bt_mute.TabIndex = 6;
            this.bt_mute.Text = "Mute";
            this.bt_mute.UseVisualStyleBackColor = true;
            this.bt_mute.Click += new System.EventHandler(this.bt_mute_Click);
            // 
            // KeyboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "KeyboardForm";
            this.Text = "Form1";
            this.panel.ResumeLayout(false);
            this.gb_media.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button bt_play;
        private System.Windows.Forms.Button bt_stop;
        private System.Windows.Forms.Button bt_next;
        private System.Windows.Forms.Button bt_previous;
        private System.Windows.Forms.Button bt_pause;
        private System.Windows.Forms.GroupBox gb_media;
        private System.Windows.Forms.Button bt_playPause;
        private System.Windows.Forms.Button bt_mute;
    }
}

