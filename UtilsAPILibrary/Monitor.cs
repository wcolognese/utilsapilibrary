﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace UtilsAPILibrary
{
    public class Monitor
    {
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        private int SC_MONITORPOWER = 0xF170;
        private int WM_SYSCOMMAND = 0x0112;

        private IntPtr Handle;

        public Monitor(IntPtr handle)
        {
            this.Handle = handle;
        }

        public enum MonitorState
        {
            ON = -1,
            OFF = 2,
            STANDBY = 1
        }

        public void SetMonitorState(MonitorState state)
        {
            SendMessage(this.Handle, WM_SYSCOMMAND, SC_MONITORPOWER, (int)state);
        }
    }
}
